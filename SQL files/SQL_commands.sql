SELECT idCliente, nombre FROM cliente ORDER BY nombre ASC;
SELECT idPrendaEspecial, descripcion, max(costoPrendaEspecial) AS costo, tipo FROM prendaespecial NATURAL JOIN cliente NATURAL JOIN lavado_prendaesp WHERE idCliente = 1 GROUP BY idPrendaEspecial ORDER BY descripcion ASC;
USE Xiance;
SHOW tables;

CALL guardarOrden( #Pepe pide todos los servicios
	/*Datos del cliente*/
    null, 'Pepe', '5518390275',
    300.00,
    150.00,
    40.00, '2017-11-24', 2, false, 
    60.00, '2017-11-22', true,
    160.00, '2017-11-24',
    40.00, '2017-11-25', 'Ajustar mangas'
);

#muestra todas las tablas unidas
SELECT * FROM orden LEFT OUTER JOIN cliente ON cliente.idCliente = orden.idCliente LEFT OUTER JOIN costura ON costura.idOrden = orden.idOrden LEFT OUTER JOIN tintoreria ON tintoreria.idOrden = orden.idOrden LEFT OUTER JOIN planchado ON planchado.idOrden = orden.idOrden LEFT OUTER JOIN lavado ON lavado.idOrden = orden.idOrden  
UNION  
SELECT * FROM orden RIGHT OUTER JOIN cliente ON cliente.idCliente = orden.idCliente RIGHT OUTER JOIN costura ON costura.idOrden = orden.idOrden RIGHT OUTER JOIN tintoreria ON tintoreria.idOrden = orden.idOrden RIGHT OUTER JOIN planchado ON planchado.idOrden = orden.idOrden RIGHT OUTER JOIN lavado ON lavado.idOrden = orden.idOrden;

SELECT * FROM orden NATURAL JOIN pago;


/*
*	CALL guardarOrden(
	idCliente, nombre, telefono,
    costo total orden
    adelanto,
    costo lavado, fecha entrega, kilos, urgencia,
    costo planchado, fecha entrega, con ganchos,
    costo tintoreria, fecha entrega,
    costo costura, fecha entrega, descripcion
*/
DELIMITER //
DROP PROCEDURE  IF EXISTS xiance.guardarOrden //
CREATE PROCEDURE guardarOrden(
	cliId INT, cliNombre VARCHAR(50), cliTelefono CHAR(10), #cliente
    ordTotal DECIMAL(8,2), #orden
    pagCantidad DECIMAL(8,2), #pago
    lavCosto DECIMAL(8,2), lavFecha DATE, lavKilos INT, lavUrgencia BOOLEAN, #lavado
    plaCosto DECIMAL(8,2), plaFecha DATE, plaGanchos BOOLEAN, #planchado
    tinCosto DECIMAL(8,2), tinFecha DATE, #tintoreria
    cosCosto DECIMAL(8,2), cosFecha DATE, cosDescripcion TINYTEXT #costura
    
)
BEGIN 
	DECLARE id INT; #id de la orden, abreviado porque se requiere frecuentemente
    DECLARE plaId INT;
    DECLARE lavId INT;
    DECLARE tinId INT;
	#cliente
	IF (cliId IS NULL) THEN #En caso de que no exista el cliente
		INSERT INTO cliente (nombre, telefono) VALUES (cliNombre, cliTelefono);
        SELECT last_insert_id() INTO cliId;
	ELSE #Si se selecciona un cliente existente
		UPDATE cliente SET nombre = cliNombre, telefono = cliTelefono WHERE idCliente = cliId;
    END IF;
    #orden (registro en la tabla orden)
	INSERT INTO orden (costo_total, idCliente) VALUES (ordtotal, cliId);
    SELECT last_insert_id() INTO id;
    #pago
    INSERT INTO pago (cantidad, idOrden) VALUES (pagCantidad, id);
    #ropa lavado
    IF (lavCosto IS NOT NULL) THEN
		INSERT INTO lavado (kilos, urgencia, fecha_entrega, costo, idOrden) VALUES (lavKilos, lavUrgencia, lavFecha, lavCosto, id);
		/*
        *	para registrar las prendas especiales se hara en el jsp, falta la instruccion correspondiente
        *
        IF( lavEspecial IS NOT NULL) THEN
            CALL prendaEspecial(last_insert_id() , lavEspecial);
		END IF;
        */
        UPDATE orden SET entregaLavado = false WHERE idOrden = id;
	END IF;
    #ropa planchado
    IF (plaCosto IS NOT NULL) THEN
        INSERT INTO planchado (ganchos, costo, fecha_entrega, idOrden) VALUES (plaGanchos, plaCosto, plaFecha, id);
        SELECT last_insert_id() INTO plaId;
        UPDATE orden SET entregaPlanchado = false WHERE idOrden = id;
        /*
        * para registrar las cantidades se manejara en el jsp, falta la instruccion correspondiente
        */
    END IF;
    #ropa tintoreria
    IF (tinCosto IS NOT NULL) THEN
		INSERT INTO tintoreria (costo, fecha_entrega, idOrden) VALUES (tinCosto, tinFecha, id);
        SELECT last_insert_id() INTO tinId;
        UPDATE orden SET entregaTintoreria = false WHERE idOrden = id;
        /*
        * para registrar las prendas de tintoreria se manejara en el jsp, falta la instruccion correspondiente
        * tomar en cuenta que se debe actualizar normalTerminado y urgenteTerminado en caso de que no haya ese tipo de prendas
        */
	END IF;
    #ropa costura
    IF (cosCosto IS NOT NULL) THEN
		INSERT INTO costura (descripcion, costo, fecha_entrega, idOrden) VALUES(cosDescripcion, cosCosto, cosFecha, id);
        UPDATE orden SET entregaCostura = false WHERE idOrden = id;
	END IF;
END //
DELIMITER ;