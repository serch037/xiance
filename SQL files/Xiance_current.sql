-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for xiance
CREATE DATABASE IF NOT EXISTS `xiance` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `xiance`;

-- Dumping structure for table xiance.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` char(10) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.costura
CREATE TABLE IF NOT EXISTS `costura` (
  `idCostura` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` tinytext DEFAULT NULL,
  `terminado` tinyint(1) DEFAULT 0,
  `costo` float DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `idOrden` int(11) NOT NULL,
  PRIMARY KEY (`idCostura`),
  KEY `idOrden` (`idOrden`),
  CONSTRAINT `costura_ibfk_1` FOREIGN KEY (`idOrden`) REFERENCES `orden` (`idOrden`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.lavado
CREATE TABLE IF NOT EXISTS `lavado` (
  `idLavado` int(11) NOT NULL AUTO_INCREMENT,
  `kilos` int(11) DEFAULT NULL,
  `urgencia` tinyint(1) DEFAULT NULL,
  `costo` float DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `terminado` tinyint(1) DEFAULT NULL,
  `idOrden` int(11) NOT NULL,
  PRIMARY KEY (`idLavado`),
  KEY `idOrden` (`idOrden`),
  CONSTRAINT `lavado_ibfk_1` FOREIGN KEY (`idOrden`) REFERENCES `orden` (`idOrden`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.lavado_prendaesp
CREATE TABLE IF NOT EXISTS `lavado_prendaesp` (
  `idLavado` int(11) NOT NULL,
  `idPrendaEspecial` int(11) NOT NULL,
  PRIMARY KEY (`idLavado`,`idPrendaEspecial`),
  KEY `idPrendaEspecial` (`idPrendaEspecial`),
  CONSTRAINT `lavado_prendaesp_ibfk_1` FOREIGN KEY (`idLavado`) REFERENCES `lavado` (`idLavado`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `lavado_prendaesp_ibfk_2` FOREIGN KEY (`idPrendaEspecial`) REFERENCES `prendaespecial` (`idPrendaEspecial`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.orden
CREATE TABLE IF NOT EXISTS `orden` (
  `idOrden` int(11) NOT NULL AUTO_INCREMENT,
  `pago` double DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `costo_total` float DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idOrden`),
  KEY `idCliente` (`idCliente`),
  CONSTRAINT `orden_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.planchado
CREATE TABLE IF NOT EXISTS `planchado` (
  `idPlanchado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(30) DEFAULT NULL,
  `ganchos` tinyint(1) DEFAULT NULL,
  `costo` float DEFAULT NULL,
  `terminado` tinyint(1) DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `cant_norm_norm` int(11) DEFAULT NULL,
  `cant_delic_norm` int(11) DEFAULT NULL,
  `cant_urg_norm` int(11) DEFAULT NULL,
  `cant_norm_largas` int(11) DEFAULT NULL,
  `cant_delic_largas` int(11) DEFAULT NULL,
  `cant_urg_largas` int(11) DEFAULT NULL,
  `idOrden` int(11) NOT NULL,
  PRIMARY KEY (`idPlanchado`),
  KEY `idOrden` (`idOrden`),
  CONSTRAINT `planchado_ibfk_1` FOREIGN KEY (`idOrden`) REFERENCES `orden` (`idOrden`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.prendaespecial
CREATE TABLE IF NOT EXISTS `prendaespecial` (
  `idPrendaEspecial` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` tinytext DEFAULT NULL,
  `costo` float DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idPrendaEspecial`),
  KEY `idCliente` (`idCliente`),
  CONSTRAINT `prendaespecial_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.prendatintoreria
CREATE TABLE IF NOT EXISTS `prendatintoreria` (
  `idPrendaTintoreria` int(11) NOT NULL AUTO_INCREMENT,
  `costo` float DEFAULT NULL,
  `urgencia` tinyint(1) DEFAULT NULL,
  `descripcion` tinytext DEFAULT NULL,
  `tipo_maq` varchar(50) DEFAULT NULL,
  `entregado_maq` date DEFAULT NULL,
  `recibido_maq` date DEFAULT NULL,
  `ticket` varchar(10) DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idPrendaTintoreria`),
  KEY `idCliente` (`idCliente`),
  CONSTRAINT `prendatintoreria_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.tintoreria
CREATE TABLE IF NOT EXISTS `tintoreria` (
  `idTintoreria` int(11) NOT NULL AUTO_INCREMENT,
  `costo` float DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `terminado` tinyint(1) DEFAULT NULL,
  `idOrden` int(11) NOT NULL,
  PRIMARY KEY (`idTintoreria`),
  KEY `idOrden` (`idOrden`),
  CONSTRAINT `tintoreria_ibfk_1` FOREIGN KEY (`idOrden`) REFERENCES `orden` (`idOrden`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.tint_prendatint
CREATE TABLE IF NOT EXISTS `tint_prendatint` (
  `idTintoreria` int(11) NOT NULL,
  `idPrendaTintoreria` int(11) NOT NULL,
  PRIMARY KEY (`idTintoreria`,`idPrendaTintoreria`),
  KEY `idPrendaTintoreria` (`idPrendaTintoreria`),
  CONSTRAINT `tint_prendatint_ibfk_1` FOREIGN KEY (`idTintoreria`) REFERENCES `tintoreria` (`idTintoreria`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tint_prendatint_ibfk_2` FOREIGN KEY (`idPrendaTintoreria`) REFERENCES `prendatintoreria` (`idPrendaTintoreria`) ON DELETE NO ACTION ON UPDATE CASCADE
) 

-- Data exporting was unselected.
-- Dumping structure for table xiance.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` text NOT NULL DEFAULT '0',
  `pass` text NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) 

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
