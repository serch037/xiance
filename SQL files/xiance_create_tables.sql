-- MySQL Workbench Forward Engineering


-- -----------------------------------------------------
-- Schema xiance
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `xiance`;
CREATE SCHEMA IF NOT EXISTS `xiance` DEFAULT CHARACTER SET utf8;
USE `xiance`;
GRANT ALL PRIVILEGES ON xiance To 'admin'@'hostname' IDENTIFIED BY 'password';
-- -----------------------------------------------------
-- Table cliente
-- -----------------------------------------------------
DROP TABLE IF EXISTS usuario ;

CREATE TABLE IF NOT EXISTS usuario (
    idUsuario INT(11) NOT NULL AUTO_INCREMENT,
    nombreUsuario VARCHAR(50) NOT NULL,
    passDigest TEXT NOT NULL,
    PRIMARY KEY (idUsuario)
);

INSERT into usuario (usuario,pass_digest) values('prueba', 1234);
-- -----------------------------------------------------
-- Table cliente
-- -----------------------------------------------------
DROP TABLE IF EXISTS cliente ;

CREATE TABLE IF NOT EXISTS cliente (
    idCliente INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50),
    telefono CHAR(10) NOT NULL,
    PRIMARY KEY (`idCliente`)
);


-- -----------------------------------------------------
-- Table orden
-- -----------------------------------------------------
DROP TABLE IF EXISTS orden ;

CREATE TABLE IF NOT EXISTS orden (
    `idOrden` INT(11) NOT NULL AUTO_INCREMENT,
    `costo_total` DECIMAL(8 , 2 ) NOT NULL,
    `idCliente` INT(11) NOT NULL,
    `fecha` DATE NOT NULL,
    `entregaTintoreria` TINYINT(1) NULL DEFAULT 1,
    `entregaLavado` TINYINT(1) NULL DEFAULT 1,
    `entregaPlanchado` TINYINT(1) NULL DEFAULT 1,
    `entregaCostura` TINYINT(1) NULL DEFAULT 1,
    PRIMARY KEY (`idOrden`),
    INDEX `idCliente` (`idCliente` ASC),
    CONSTRAINT `orden_ibfk_1` FOREIGN KEY (`idCliente`)
        REFERENCES `cliente` (`idCliente`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `costura_servicio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS costura_servicio ;
CREATE TABLE IF NOT EXISTS costura_servicio (
  `idServicio` INT(11) NOT NULL AUTO_INCREMENT,
  `idCostura` INT NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  `costo` DECIMAL(8 , 2 ) NOT NULL,
  PRIMARY KEY (`idServicio`),
  INDEX `idServicio` (`idServicio` ASC),
  CONSTRAINT `servicio_ibfk_1` FOREIGN KEY (`idCostura`)
        REFERENCES `costura` (`idCostura`)
        ON DELETE NO ACTION ON UPDATE CASCADE
  );

-- -----------------------------------------------------
-- Table `costura`
-- -----------------------------------------------------
DROP TABLE IF EXISTS costura ;

CREATE TABLE IF NOT EXISTS costura (
    `idCostura` INT(11) NOT NULL AUTO_INCREMENT,
    `costo` DECIMAL(8 , 2 ) NOT NULL,
    `fecha_entrega` DATE NOT NULL,
    `idOrden` INT(11) NOT NULL,
    `terminado` TINYINT(1) NULL DEFAULT 0,
    PRIMARY KEY (`idCostura`),
    INDEX `idOrden` (`idOrden` ASC),
    CONSTRAINT `costura_ibfk_1` FOREIGN KEY (`idOrden`)
        REFERENCES `orden` (`idOrden`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `detalleplanchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS detalleplanchado ;

CREATE TABLE IF NOT EXISTS detalleplanchado (
    `idDetallePlanchado` INT(11) NOT NULL AUTO_INCREMENT,
    `cantidad` INT(11) NOT NULL,
    `larga` TINYINT(1) NOT NULL,
    `delicada` TINYINT(1) NOT NULL,
    `urgente` TINYINT(1) NOT NULL,
    PRIMARY KEY (`idDetallePlanchado`)
);


-- -----------------------------------------------------
-- Table `lavado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS lavado ;

CREATE TABLE IF NOT EXISTS lavado (
    `idLavado` INT(11) NOT NULL AUTO_INCREMENT,
    `kilos` INT(11) NOT NULL,
    `urgencia` TINYINT(1) NOT NULL,
    `fecha_entrega` DATE NOT NULL,
    `costo` DECIMAL(8 , 2 ) NOT NULL,
    `idOrden` INT(11) NOT NULL,
    `terminado` TINYINT(1) NULL DEFAULT '0',
    `lavadora` INT(11) NULL DEFAULT NULL,
    `ordenLavado` INT(11) NULL DEFAULT NULL,
    PRIMARY KEY (`idLavado`),
    INDEX `idOrden` (`idOrden` ASC),
    CONSTRAINT `lavado_ibfk_1` FOREIGN KEY (`idOrden`)
        REFERENCES `orden` (`idOrden`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `prendaespecial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS prendaespecial ;

CREATE TABLE IF NOT EXISTS `prendaespecial` (
    `idPrendaEspecial` INT(11) NOT NULL AUTO_INCREMENT,
    `descripcion` TINYTEXT NOT NULL,
    `tipo` ENUM('cobija', 'edredon', 'cobertor', 'tenis', 'almohada', 'cortina') NOT NULL,
    `idCliente` INT(11) NOT NULL,
    PRIMARY KEY (`idPrendaEspecial`),
    INDEX `idCliente` (`idCliente` ASC),
    CONSTRAINT `prendaespecial_ibfk_1` FOREIGN KEY (`idCliente`)
        REFERENCES `cliente` (`idCliente`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `lavado_prendaesp`
-- -----------------------------------------------------
DROP TABLE IF EXISTS lavado_prendaesp ;

CREATE TABLE IF NOT EXISTS lavado_prendaesp (
    `idLavado` INT(11) NOT NULL,
    `idPrendaEspecial` INT(11) NOT NULL,
    `costoPrendaEspecial` DECIMAL(8 , 2 ) NOT NULL,
    PRIMARY KEY (`idLavado` , `idPrendaEspecial`),
    INDEX `idPrendaEspecial` (`idPrendaEspecial` ASC),
    CONSTRAINT `lavado_prendaesp_ibfk_1` FOREIGN KEY (`idLavado`)
        REFERENCES `lavado` (`idLavado`)
        ON DELETE NO ACTION ON UPDATE CASCADE,
    CONSTRAINT `lavado_prendaesp_ibfk_2` FOREIGN KEY (`idPrendaEspecial`)
        REFERENCES `prendaespecial` (`idPrendaEspecial`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `pago`
-- -----------------------------------------------------
DROP TABLE IF EXISTS pago ;

CREATE TABLE IF NOT EXISTS `pago` (
    `idPago` INT(11) NOT NULL AUTO_INCREMENT,
    `cantidad` DECIMAL(8 , 2 ) NOT NULL,
    `idOrden` INT(11) NOT NULL,
    `fechaPago` DATE NOT NULL,
    PRIMARY KEY (`idPago`),
    INDEX `idOrden` (`idOrden` ASC),
    CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`idOrden`)
        REFERENCES `orden` (`idOrden`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `planchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS planchado ;

CREATE TABLE IF NOT EXISTS planchado (
    `idPlanchado` INT(11) NOT NULL AUTO_INCREMENT,
    `ganchos` TINYINT(1) NOT NULL,
    `costo` DECIMAL(8 , 2 ) NOT NULL,
    `fecha_entrega` DATE NOT NULL,
    `idOrden` INT(11) NOT NULL,
    `normalTerminado` TINYINT(1) NULL DEFAULT '0',
    `urgenteTerminado` TINYINT(1) NULL DEFAULT '0',
    PRIMARY KEY (`idPlanchado`),
    INDEX `idOrden` (`idOrden` ASC),
    CONSTRAINT `planchado_ibfk_1` FOREIGN KEY (`idOrden`)
        REFERENCES `orden` (`idOrden`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `planchado_detalleplanchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS planchado_detalleplanchado ;

CREATE TABLE IF NOT EXISTS `planchado_detalleplanchado` (
    `idDetallePlanchado` INT(11) NOT NULL,
    `idPlanchado` INT(11) NOT NULL,
    PRIMARY KEY (`idDetallePlanchado` , `idPlanchado`),
    INDEX `fk_detallePlanchado_has_planchado_planchado1_idx` (`idPlanchado` ASC),
    INDEX `fk_detallePlanchado_has_planchado_detallePlanchado_idx` (`idDetallePlanchado` ASC),
    CONSTRAINT `fk_detallePlanchado_has_planchado_detallePlanchado` FOREIGN KEY (`idDetallePlanchado`)
        REFERENCES `detalleplanchado` (`idDetallePlanchado`)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `fk_detallePlanchado_has_planchado_planchado1` FOREIGN KEY (`idPlanchado`)
        REFERENCES `planchado` (`idPlanchado`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `prendatintoreria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS prendatintoreria ;

CREATE TABLE IF NOT EXISTS prendatintoreria (
    `idPrendaTintoreria` INT(11) NOT NULL AUTO_INCREMENT,
    `ticket` VARCHAR(10) NULL,
    `descripcion` TINYTEXT NOT NULL,
    `tipo` ENUM('camisa', 'pantalon', 'saco', 'sueter', 'corbata', 'sudadera', 'chaleco', 'vestido', 'chamarra', 'abrigo', 'gabardina', 'edredon normal', 'edredon plumas', 'vestido de fiesta') NOT NULL,
	`tipo_maq` ENUM('a','b') NOT NULL,
    `urgencia` TINYINT(1) NOT NULL,
    `entregado_maq` DATE NULL DEFAULT NULL,
    `recibido_maq` DATE NULL DEFAULT NULL,
    `idCliente` INT(11) NOT NULL,
    PRIMARY KEY (`idPrendaTintoreria`),
    INDEX `idCliente` (`idCliente` ASC),
    CONSTRAINT `prendatintoreria_ibfk_1` FOREIGN KEY (`idCliente`)
        REFERENCES `cliente` (`idCliente`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `reporte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `reporte` ;

CREATE TABLE IF NOT EXISTS reporte (
    `idReporte` INT(11) NOT NULL AUTO_INCREMENT,
    `cajaUno` DECIMAL(8 , 2 ) NULL DEFAULT NULL,
    `cajaDos` DECIMAL(8 , 2 ) NULL DEFAULT NULL,
    `cajaTres` DECIMAL(8 , 2 ) NULL DEFAULT NULL,
    `fechaReporte` DATE NULL DEFAULT NULL,
    PRIMARY KEY (`idReporte`)
);


-- -----------------------------------------------------
-- Table `tintoreria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS tintoreria ;

CREATE TABLE IF NOT EXISTS `tintoreria` (
    `idTintoreria` INT(11) NOT NULL AUTO_INCREMENT,
    `costo` DECIMAL(8 , 2 ) NOT NULL,
    `fecha_entrega` DATE NOT NULL,
    `idOrden` INT(11) NOT NULL,
    PRIMARY KEY (`idTintoreria`),
    INDEX `idOrden` (`idOrden` ASC),
    CONSTRAINT `tintoreria_ibfk_1` FOREIGN KEY (`idOrden`)
        REFERENCES `orden` (`idOrden`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table `tint_prendatint`
-- -----------------------------------------------------
DROP TABLE IF EXISTS tint_prendatint ;

CREATE TABLE IF NOT EXISTS tint_prendatint (
    `idTintoreria` INT(11) NOT NULL,
    `idPrendaTintoreria` INT(11) NOT NULL,
    `costoPrendaTintoreria` DECIMAL(8 , 2 ) NULL DEFAULT NULL,
    PRIMARY KEY (`idTintoreria` , `idPrendaTintoreria`),
    INDEX `idPrendaTintoreria` (`idPrendaTintoreria` ASC),
    CONSTRAINT `tint_prendatint_ibfk_1` FOREIGN KEY (`idTintoreria`)
        REFERENCES `tintoreria` (`idTintoreria`)
        ON DELETE NO ACTION ON UPDATE CASCADE,
    CONSTRAINT `tint_prendatint_ibfk_2` FOREIGN KEY (`idPrendaTintoreria`)
        REFERENCES `prendatintoreria` (`idPrendaTintoreria`)
        ON DELETE NO ACTION ON UPDATE CASCADE
);

/*DELIMITER $$

USE `xiance`$$
DROP TRIGGER IF EXISTS `orden_BEFORE_INSERT` $$
CREATE
DEFINER=`vel`@`localhost`
TRIGGER `orden_BEFORE_INSERT`
BEFORE INSERT ON `orden`
FOR EACH ROW
BEGIN
	SET new.fecha = now();
END$$
*/

DELIMITER $$
USE `xiance`$$
DROP TRIGGER IF EXISTS `orden_BEFORE_INSERT` $$
CREATE DEFINER=CURRENT_USER TRIGGER `orden_BEFORE_INSERT` BEFORE INSERT ON `orden`
FOR EACH ROW
BEGIN
	SET new.fecha = now();
END$$


DROP TRIGGER IF EXISTS `pago_BEFORE_INSERT` $$
CREATE DEFINER = CURRENT_USER TRIGGER `pago_BEFORE_INSERT` BEFORE INSERT ON `pago`
FOR EACH ROW
BEGIN
	SET new.fechapago = now();
END$$



DELIMITER //
DROP PROCEDURE  IF EXISTS xiance.guardarOrden //
CREATE PROCEDURE guardarOrden(
	cliId INT, #cliente
    ordTotal DECIMAL(8,2), #orden
    pagCantidad DECIMAL(8,2), #pago
    lavCosto DECIMAL(8,2), lavFecha DATE, lavKilos INT, lavUrgencia BOOLEAN, #lavado
    plaCosto DECIMAL(8,2), plaFecha DATE, plaGanchos BOOLEAN, #planchado
    tinCosto DECIMAL(8,2), tinFecha DATE, #tintoreria
    cosCosto DECIMAL(8,2), cosFecha DATE, #costura
    out id INT, out lavId INT, out plaId INT, out tinId INT, out cosId INT
)
BEGIN 
	#DECLARE id INT; #id de la orden, abreviado porque se requiere frecuentemente
    #DECLARE plaId INT;
    #DECLARE lavId INT;
    #DECLARE tinId INT;	
    #orden (registro en la tabla orden)
	INSERT INTO orden (costo_total, idCliente) VALUES (ordtotal, cliId);
    SELECT last_insert_id() INTO id;
    #pago
    INSERT INTO pago (cantidad, idOrden) VALUES (pagCantidad, id);
    #ropa lavado
    IF (lavCosto > 0) THEN
		INSERT INTO lavado (kilos, urgencia, fecha_entrega, costo, idOrden) VALUES (lavKilos, lavUrgencia, lavFecha, lavCosto, id);
		/*
        *	para registrar las prendas especiales se hara en el jsp, falta la instruccion correspondiente
        *
        IF( lavEspecial IS NOT NULL) THEN
            CALL prendaEspecial(last_insert_id() , lavEspecial);
		END IF;
        */
		SELECT last_insert_id() INTO lavId;
        UPDATE orden SET entregaLavado = false WHERE idOrden = id;
	END IF;
    #ropa planchado
    IF (plaCosto >0 ) THEN
        INSERT INTO planchado (ganchos, costo, fecha_entrega, idOrden) VALUES (plaGanchos, plaCosto, plaFecha, id);
        SELECT last_insert_id() INTO plaId;
        UPDATE orden SET entregaPlanchado = false WHERE idOrden = id;
        /*
        * para registrar las cantidades se manejara en el jsp, falta la instruccion correspondiente
        */
    END IF;
    #ropa tintoreria
    IF (tinCosto >0) THEN
		INSERT INTO tintoreria (costo, fecha_entrega, idOrden) VALUES (tinCosto, tinFecha, id);
        SELECT last_insert_id() INTO tinId;
        UPDATE orden SET entregaTintoreria = false WHERE idOrden = id;
        /*
        * para registrar las prendas de tintoreria se manejara en el jsp, falta la instruccion correspondiente
        * tomar en cuenta que se debe actualizar normalTerminado y urgenteTerminado en caso de que no haya ese tipo de prendas
        */
	END IF;
    #ropa costura
    IF (cosCosto >0) THEN
		INSERT INTO costura (costo, fecha_entrega, idOrden) VALUES (cosCosto, cosFecha, id);
        SELECT last_insert_id() INTO cosId;
        UPDATE orden SET entregaCostura = false WHERE idOrden = id;
	END IF;
END //
DELIMITER ;