-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema xiance
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema xiance
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `xiance` DEFAULT CHARACTER SET utf8 ;
USE `xiance` ;

-- -----------------------------------------------------
-- Table `xiance`.`cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`cliente` ;

CREATE TABLE IF NOT EXISTS `xiance`.`cliente` (
  `idCliente` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `telefono` CHAR(10) NOT NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`orden`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`orden` ;

CREATE TABLE IF NOT EXISTS `xiance`.`orden` (
  `idOrden` INT(11) NOT NULL AUTO_INCREMENT,
  `costo_total` DECIMAL(8,2) NOT NULL,
  `idCliente` INT(11) NOT NULL,
  `fecha` DATE NULL DEFAULT NULL,
  `entregaTintoreria` TINYINT(1) NULL DEFAULT 1,
  `entregaLavado` TINYINT(1) NULL DEFAULT 1,
  `entregaPlanchado` TINYINT(1) NULL DEFAULT 1,
  `entregaCostura` TINYINT(1) NULL DEFAULT 1,
  PRIMARY KEY (`idOrden`),
  INDEX `idCliente` (`idCliente` ASC),
  CONSTRAINT `orden_ibfk_1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `xiance`.`cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`costura`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`costura` ;

CREATE TABLE IF NOT EXISTS `xiance`.`costura` (
  `idCostura` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` TINYTEXT NOT NULL,
  `costo` DECIMAL(8,2) NOT NULL,
  `fecha_entrega` DATE NOT NULL,
  `idOrden` INT(11) NOT NULL,
  `terminado` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idCostura`),
  INDEX `idOrden` (`idOrden` ASC),
  CONSTRAINT `costura_ibfk_1`
    FOREIGN KEY (`idOrden`)
    REFERENCES `xiance`.`orden` (`idOrden`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`detalleplanchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`detalleplanchado` ;

CREATE TABLE IF NOT EXISTS `xiance`.`detalleplanchado` (
  `idDetallePlanchado` INT(11) NOT NULL AUTO_INCREMENT,
  `cantidad` INT(11) NOT NULL,
  `larga` TINYINT(1) NOT NULL,
  `delicada` TINYINT(1) NOT NULL,
  `urgente` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idDetallePlanchado`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`lavado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`lavado` ;

CREATE TABLE IF NOT EXISTS `xiance`.`lavado` (
  `idLavado` INT(11) NOT NULL AUTO_INCREMENT,
  `kilos` INT(11) NOT NULL,
  `urgencia` TINYINT(1) NOT NULL,
  `fecha_entrega` DATE NOT NULL,
  `costo` DECIMAL(8,2) NOT NULL,
  `idOrden` INT(11) NOT NULL,
  `terminado` TINYINT(1) NULL DEFAULT '0',
  `lavadora` INT(11) NULL DEFAULT NULL,
  `ordenLavado` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idLavado`),
  INDEX `idOrden` (`idOrden` ASC),
  CONSTRAINT `lavado_ibfk_1`
    FOREIGN KEY (`idOrden`)
    REFERENCES `xiance`.`orden` (`idOrden`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`prendaespecial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`prendaespecial` ;

CREATE TABLE IF NOT EXISTS `xiance`.`prendaespecial` (
  `idPrendaEspecial` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` TINYTEXT NOT NULL,
  `tipo` ENUM('cobija', 'edredón', 'cobertor', 'tenis', 'almohada', 'cortina') NOT NULL,
  `idCliente` INT(11) NOT NULL,
  PRIMARY KEY (`idPrendaEspecial`),
  INDEX `idCliente` (`idCliente` ASC),
  CONSTRAINT `prendaespecial_ibfk_1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `xiance`.`cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`lavado_prendaesp`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`lavado_prendaesp` ;

CREATE TABLE IF NOT EXISTS `xiance`.`lavado_prendaesp` (
  `idLavado` INT(11) NOT NULL,
  `idPrendaEspecial` INT(11) NOT NULL,
  `costoPrendaEspecial` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`idLavado`, `idPrendaEspecial`),
  INDEX `idPrendaEspecial` (`idPrendaEspecial` ASC),
  CONSTRAINT `lavado_prendaesp_ibfk_1`
    FOREIGN KEY (`idLavado`)
    REFERENCES `xiance`.`lavado` (`idLavado`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `lavado_prendaesp_ibfk_2`
    FOREIGN KEY (`idPrendaEspecial`)
    REFERENCES `xiance`.`prendaespecial` (`idPrendaEspecial`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`pago`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`pago` ;

CREATE TABLE IF NOT EXISTS `xiance`.`pago` (
  `idPago` INT(11) NOT NULL AUTO_INCREMENT,
  `cantidad` DECIMAL(8,2) NOT NULL,
  `idOrden` INT(11) NOT NULL,
  `fechaPago` DATE NULL,
  PRIMARY KEY (`idPago`),
  INDEX `idOrden` (`idOrden` ASC),
  CONSTRAINT `pago_ibfk_1`
    FOREIGN KEY (`idOrden`)
    REFERENCES `xiance`.`orden` (`idOrden`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`planchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`planchado` ;

CREATE TABLE IF NOT EXISTS `xiance`.`planchado` (
  `idPlanchado` INT(11) NOT NULL AUTO_INCREMENT,
  `ganchos` TINYINT(1) NOT NULL,
  `costo` DECIMAL(8,2) NOT NULL,
  `fecha_entrega` DATE NOT NULL,
  `idOrden` INT(11) NOT NULL,
  `normalTerminado` TINYINT(1) NULL DEFAULT '0',
  `urgenteTerminado` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`idPlanchado`),
  INDEX `idOrden` (`idOrden` ASC),
  CONSTRAINT `planchado_ibfk_1`
    FOREIGN KEY (`idOrden`)
    REFERENCES `xiance`.`orden` (`idOrden`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`planchado_detalleplanchado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`planchado_detalleplanchado` ;

CREATE TABLE IF NOT EXISTS `xiance`.`planchado_detalleplanchado` (
  `idDetallePlanchado` INT(11) NOT NULL,
  `idPlanchado` INT(11) NOT NULL,
  INDEX `fk_detallePlanchado_has_planchado_planchado1_idx` (`idPlanchado` ASC),
  INDEX `fk_detallePlanchado_has_planchado_detallePlanchado_idx` (`idDetallePlanchado` ASC),
  CONSTRAINT `fk_detallePlanchado_has_planchado_detallePlanchado`
    FOREIGN KEY (`idDetallePlanchado`)
    REFERENCES `xiance`.`detalleplanchado` (`idDetallePlanchado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detallePlanchado_has_planchado_planchado1`
    FOREIGN KEY (`idPlanchado`)
    REFERENCES `xiance`.`planchado` (`idPlanchado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`prendatintoreria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`prendatintoreria` ;

CREATE TABLE IF NOT EXISTS `xiance`.`prendatintoreria` (
  `idPrendaTintoreria` INT(11) NOT NULL AUTO_INCREMENT,
  `ticket` VARCHAR(10) NOT NULL,
  `descripcion` TINYTEXT NOT NULL,
  `tipo_maq` ENUM('camisa', 'pantalón', 'saco', 'sueter', 'corbata', 'sudadera', 'chaleco', 'vestido', 'chamarra', 'abrigo', 'gabardina', 'edredón normal', 'edredón plumas', 'vestido de fiesta') NOT NULL,
  `urgencia` TINYINT(1) NOT NULL,
  `entregado_maq` DATE NULL DEFAULT NULL,
  `recibido_maq` DATE NULL DEFAULT NULL,
  `idCliente` INT(11) NOT NULL,
  PRIMARY KEY (`idPrendaTintoreria`),
  INDEX `idCliente` (`idCliente` ASC),
  CONSTRAINT `prendatintoreria_ibfk_1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `xiance`.`cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`reporte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`reporte` ;

CREATE TABLE IF NOT EXISTS `xiance`.`reporte` (
  `idReporte` INT(11) NOT NULL AUTO_INCREMENT,
  `cajaUno` DECIMAL(8,2) NULL DEFAULT NULL,
  `cajaDos` DECIMAL(8,2) NULL DEFAULT NULL,
  `cajaTres` DECIMAL(8,2) NULL DEFAULT NULL,
  `fechaReporte` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`idReporte`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`tintoreria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`tintoreria` ;

CREATE TABLE IF NOT EXISTS `xiance`.`tintoreria` (
  `idTintoreria` INT(11) NOT NULL AUTO_INCREMENT,
  `costo` DECIMAL(8,2) NOT NULL,
  `fecha_entrega` DATE NOT NULL,
  `idOrden` INT(11) NOT NULL,
  PRIMARY KEY (`idTintoreria`),
  INDEX `idOrden` (`idOrden` ASC),
  CONSTRAINT `tintoreria_ibfk_1`
    FOREIGN KEY (`idOrden`)
    REFERENCES `xiance`.`orden` (`idOrden`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `xiance`.`tint_prendatint`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `xiance`.`tint_prendatint` ;

CREATE TABLE IF NOT EXISTS `xiance`.`tint_prendatint` (
  `idTintoreria` INT(11) NOT NULL,
  `idPrendaTintoreria` INT(11) NOT NULL,
  `costoPrendaTintoreria` DECIMAL(8,2) NULL DEFAULT NULL,
  PRIMARY KEY (`idTintoreria`, `idPrendaTintoreria`),
  INDEX `idPrendaTintoreria` (`idPrendaTintoreria` ASC),
  CONSTRAINT `tint_prendatint_ibfk_1`
    FOREIGN KEY (`idTintoreria`)
    REFERENCES `xiance`.`tintoreria` (`idTintoreria`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `tint_prendatint_ibfk_2`
    FOREIGN KEY (`idPrendaTintoreria`)
    REFERENCES `xiance`.`prendatintoreria` (`idPrendaTintoreria`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `xiance`;

DELIMITER $$

USE `xiance`$$
DROP TRIGGER IF EXISTS `xiance`.`orden_BEFORE_INSERT` $$
USE `xiance`$$
CREATE
DEFINER=`vel`@`localhost`
TRIGGER `xiance`.`orden_BEFORE_INSERT`
BEFORE INSERT ON `xiance`.`orden`
FOR EACH ROW
BEGIN
	SET new.fecha = now();
END$$


USE `xiance`$$
DROP TRIGGER IF EXISTS `xiance`.`pago_BEFORE_INSERT` $$
USE `xiance`$$
CREATE DEFINER = CURRENT_USER TRIGGER `xiance`.`pago_BEFORE_INSERT` BEFORE INSERT ON `pago` FOR EACH ROW
BEGIN
	SET new.fechapago = now();
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

DELIMITER //
DROP PROCEDURE  IF EXISTS xiance.guardarOrden //
CREATE PROCEDURE guardarOrden(
	cliId INT, cliNombre VARCHAR(50), cliTelefono CHAR(10), #cliente
    ordTotal DECIMAL(8,2), #orden
    pagCantidad DECIMAL(8,2), #pago
    lavCosto DECIMAL(8,2), lavFecha DATE, lavKilos INT, lavUrgencia BOOLEAN, #lavado
    plaCosto DECIMAL(8,2), plaFecha DATE, plaGanchos BOOLEAN, #planchado
    tinCosto DECIMAL(8,2), tinFecha DATE, #tintoreria
    cosCosto DECIMAL(8,2), cosFecha DATE, cosDescripcion TINYTEXT #costura
    
)
BEGIN 
	DECLARE id INT; #id de la orden, abreviado porque se requiere frecuentemente
    DECLARE plaId INT;
    DECLARE lavId INT;
    DECLARE tinId INT;
	#cliente
	IF (cliId IS NULL) THEN #En caso de que no exista el cliente
		INSERT INTO cliente (nombre, telefono) VALUES (cliNombre, cliTelefono);
        SELECT last_insert_id() INTO cliId;
	ELSE #Si se selecciona un cliente existente
		UPDATE cliente SET nombre = cliNombre, telefono = cliTelefono WHERE idCliente = cliId;
    END IF;
    #orden (registro en la tabla orden)
	INSERT INTO orden (costo_total, idCliente) VALUES (ordtotal, cliId);
    SELECT last_insert_id() INTO id;
    #pago
    INSERT INTO pago (cantidad, idOrden) VALUES (pagCantidad, id);
    #ropa lavado
    IF (lavCosto IS NOT NULL) THEN
		INSERT INTO lavado (kilos, urgencia, fecha_entrega, costo, idOrden) VALUES (lavKilos, lavUrgencia, lavFecha, lavCosto, id);
		/*
        *	para registrar las prendas especiales se hara en el jsp, falta la instruccion correspondiente
        *
        IF( lavEspecial IS NOT NULL) THEN
            CALL prendaEspecial(last_insert_id() , lavEspecial);
		END IF;
        */
        UPDATE orden SET entregaLavado = false WHERE idOrden = id;
	END IF;
    #ropa planchado
    IF (plaCosto IS NOT NULL) THEN
        INSERT INTO planchado (ganchos, costo, fecha_entrega, idOrden) VALUES (plaGanchos, plaCosto, plaFecha, id);
        SELECT last_insert_id() INTO plaId;
        UPDATE orden SET entregaPlanchado = false WHERE idOrden = id;
        /*
        * para registrar las cantidades se manejara en el jsp, falta la instruccion correspondiente
        */
    END IF;
    #ropa tintoreria
    IF (tinCosto IS NOT NULL) THEN
		INSERT INTO tintoreria (costo, fecha_entrega, idOrden) VALUES (tinCosto, tinFecha, id);
        SELECT last_insert_id() INTO tinId;
        UPDATE orden SET entregaTintoreria = false WHERE idOrden = id;
        /*
        * para registrar las prendas de tintoreria se manejara en el jsp, falta la instruccion correspondiente
        * tomar en cuenta que se debe actualizar normalTerminado y urgenteTerminado en caso de que no haya ese tipo de prendas
        */
	END IF;
    #ropa costura
    IF (cosCosto IS NOT NULL) THEN
		INSERT INTO costura (descripcion, costo, fecha_entrega, idOrden) VALUES(cosDescripcion, cosCosto, cosFecha, id);
        UPDATE orden SET entregaCostura = false WHERE idOrden = id;
	END IF;
END //
DELIMITER ;