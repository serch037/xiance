import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import javax.servlet.annotation.WebServlet;

@WebServlet("/getUser")
public class GetUser extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse res){

		try{

			String value = req.getParameter("user");

			res.setContentType("text/html");
			PrintWriter out = res.getWriter();

			out.println("<html><body>Hi " + value + "</body></html>");
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}