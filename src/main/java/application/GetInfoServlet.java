package application;

import com.google.gson.Gson;
import database.OrdenDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GetInfoServlet")
public class GetInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        System.out.println("GET");
        log("GET");
        if (ajax) {
            System.out.println("AJAX");
            response.setContentType("application/json");
            if (action.equals("findLavado")) {
                String json = new Gson().toJson(OrdenDB.findLavadosIncompletos());
            } else if (action.equals("findPlanchado")) {

            } else if (action.equals("findTintoreria")) {

            } else if (action.equals("findCostura")) {

            }
        }
    }
}
