package application;

import business.User;
import database.UserDB;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@javax.servlet.annotation.WebServlet(name = "LoginServlet", urlPatterns = {"/login", "/secured/logout"})
public class LoginServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String action = "login";
        action = request.getParameter("action");
        if (action.equals("login")) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String message;
            String url = "/index.jsp";

            User user = UserDB.getUser(username);
            if (user == null) {
                message = "This user does not exist.<br>";
                request.setAttribute("message", message);
                getServletContext()
                            .getRequestDispatcher(url)
                            .forward(request, response);
            } else {
                message = "This user exists.<br>";
                if (user.authenticate(password)) {
                    request.getSession().setAttribute("user", user);
                    response.sendRedirect("/secured/recepcion.jsp");
                } else {
                    message = "The password is incorrect";
                    request.setAttribute("message", message);
                    getServletContext()
                            .getRequestDispatcher(url)
                            .forward(request, response);
                }
            }
        } else if (action.equals("logout")) {
            request.getSession(false).invalidate();
            response.sendRedirect("/index.jsp");
        }
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
