//TODO: Fechas de entrega
//TODO: Agregar solo si no es nulo
package application;

import business.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import database.*;
import javafx.application.Platform;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.reflect.Type;

@WebServlet(name = "OrdenServlet", urlPatterns = {"/fillOrden"})
public class OrdenServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("POST");
        log("POST");
        String action = request.getParameter("action");
        System.out.println(action);
        if (action.equals("fillOrden")) {
            System.out.println("Filling orden");
            Cliente cliente = (Cliente) request.getSession(false).
                    getAttribute("cliente");
            int idCliente = cliente.getId();
            Gson gson = new Gson();

            //Para lavado
            Lavado lavado = new Lavado();
            String kilosLavado = request.getParameter("kilosLavado");
            String urgenciaLavado = request.getParameter("urgenciaLavado");
            LocalDate fecha = LocalDate.now();
            if (urgenciaLavado.equals("false")) {
                fecha = fecha.plusDays(1);
            }
            PrendaLavado[] lavadosEspeciales = gson.fromJson(request.getParameter("lavadosEspeciales"), PrendaLavado[].class);

            lavado.setFechaEntrega(fecha);
            if (!kilosLavado.isEmpty()) {
                lavado.setKilos(Double.parseDouble(kilosLavado));
            }
            lavado.setLavadosEspeciales(lavadosEspeciales);
            lavado.setLavadosEspecialesClienteID(idCliente);
            lavado.setUrgencia(Boolean.parseBoolean(urgenciaLavado));

            //Para planchado
            Planchado planchado = new Planchado();
            String plaGancho = request.getParameter("plaGancho");
            String plaNorNor = request.getParameter("plaNorNor");
            String plaNorUrg = request.getParameter("plaNorUrg");
            String plaNorDel = request.getParameter("plaNorDel");
            String plaNorDelUrg = request.getParameter("plaNorDelUrg");
            String plaLaNor = request.getParameter("plaLaNor");
            String plaLaUrg = request.getParameter("plaLaUrg");
            String plaLaDel = request.getParameter("plaLaDel");
            String plaLaDelUrg = request.getParameter("plaLaDelUrg");

            planchado.setFechaEntrega(LocalDate.now());
            planchado.setGancho(Boolean.parseBoolean(plaGancho));

            //Detalles planchado
            double test;
            Planchado.Detalle detalle;
            if (!plaLaNor.isEmpty() &&
                    (test = Double.parseDouble(plaNorNor)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                planchado.addDetalle(detalle);
            }
            if (!plaNorUrg.isEmpty() &&
                    (test = Double.parseDouble(plaNorUrg)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setUrgente(true);
                planchado.addDetalle(detalle);
            }
            if (!plaNorDel.isEmpty() &&
                    (test = Double.parseDouble(plaNorDel)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setDelicada(true);
                planchado.addDetalle(detalle);
            }
            if (!plaNorDelUrg.isEmpty() &&
                    (test = Double.parseDouble(plaNorDelUrg)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setDelicada(true);
                detalle.setUrgente(true);
                planchado.addDetalle(detalle);
            }
            if (!plaLaNor.isEmpty() &&
                    (test = Double.parseDouble(plaLaNor)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setLarga(true);
                planchado.addDetalle(detalle);
            }
            if (!plaLaUrg.isEmpty() &&
                    (test = Double.parseDouble(plaLaUrg)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setLarga(true);
                detalle.setUrgente(true);
                planchado.addDetalle(detalle);
            }
            if (!plaLaDel.isEmpty() &&
                    (test = Double.parseDouble(plaLaDel)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setLarga(true);
                detalle.setDelicada(true);
                planchado.addDetalle(detalle);
            }
            if (!plaLaDelUrg.isEmpty() &&
                    (test = Double.parseDouble(plaLaDelUrg)) != 0) {
                detalle = new Planchado.Detalle();
                detalle.setCantidad(test);
                detalle.setLarga(true);
                detalle.setDelicada(true);
                detalle.setUrgente(true);
                planchado.addDetalle(detalle);
            }

            // Para tintoreria
            Tintoreria tintoreria = new Tintoreria();
            Tintoreria.Prenda[] prendasTintoreria = gson.fromJson(request.getParameter("prendasTintoreria"), Tintoreria.Prenda[].class);
            tintoreria.setFechaEntrega(LocalDate.now());
            tintoreria.setPrendas(prendasTintoreria);

            //Para costura
            Costura costura = new Costura();
            costura.setFechaEntrega(LocalDate.now());
            Costura.Servicio[] servicios = gson.fromJson(request.getParameter("costura"), Costura.Servicio[].class);
            costura.setServicios(servicios);


            // Hacer Orden
            System.out.println(lavado.getCosto());
            System.out.println(planchado.getCosto());
            System.out.println(tintoreria.getCosto());
            HashMap<String, Integer> ids = OrdenDB.fillOrden(idCliente, lavado, planchado, tintoreria, costura);

            //Lavados Especiales
            LavadoDB.addLavadosEspeciales(ids.get("idLavado"), lavadosEspeciales);
            //Detalles
            PlanchadoDB.addDetalles(ids.get("idPlanchado"), planchado.getDetalles());
            //Prendas Tintoreria
            TintoreriaDB.addPrendas(idCliente, ids.get("idTintoreria"), tintoreria.getPrendas());
            //Servicios costura
            CosturaDB.addServicios(ids.get("idCostura"), costura.getServicios());
            System.out.println("Done");
        } else if (action.equals("selectClient")) {
            String url = "/recepcion.jsp";
            String nomCliente = request.getParameter("nomCliente");
            String apCliente = request.getParameter("apCliente");
            String telCliente = request.getParameter("telCliente");
            String idClienteStr = request.getParameter("idCliente");
            String modified = request.getParameter("modified");
            int idCliente;

            if (!idClienteStr.isEmpty()) {
                idCliente = Integer.parseInt(idClienteStr);
                if (Boolean.parseBoolean(modified))
                    ClienteDB.updateClient(idCliente, nomCliente, apCliente, telCliente);
            } else {
                idCliente = ClienteDB.createClient(nomCliente, apCliente, telCliente);
            }

            Cliente cliente = new Cliente();
            cliente.setNombre(nomCliente);
            cliente.setApellidos(apCliente);
            cliente.setTelefono(telCliente);
            cliente.setId(idCliente);

            HttpSession session = request.getSession(true);
            session.setAttribute("cliente", cliente);
            response.setHeader("Content-Type", "text/plain");
            PrintWriter writer = response.getWriter();
            String base = ServerUtil.getBaseURL(request);
            writer.write(base+url);
            System.out.println("Done");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        System.out.println("GET");
        log("GET");
        if (action.equals("findClients")) {
            log("findClients");
            String name = request.getParameter("nomCliente");
            ArrayList<Cliente> clientes = ClienteDB.findClientsByName(name);
            if (ajax) {
                System.out.println("AJAX");
                String json = new Gson().toJson(clientes);
                response.setContentType("application/json");
                response.getWriter().write(json);
            } else {
                String url = "/cliente.jsp";
                String message = "";
                if (clientes.isEmpty()) {
                    message = "No se encontraron clientes con el nombre especificado";
                    request.setAttribute("message", message);
                } else {
                    request.setAttribute("clientes", clientes);
                }
                getServletContext().getRequestDispatcher(url).forward(request, response);
            }
        }
    }
}
