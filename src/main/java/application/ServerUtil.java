package application;

import javax.servlet.http.HttpServletRequest;

public class ServerUtil {
    public static String getBaseURL(HttpServletRequest request) {
        String uri = request.getScheme() + "://" +
                request.getServerName() +
                ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort());
        return uri;
    }
}
