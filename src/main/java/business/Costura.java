package business;

import java.time.LocalDate;

public class Costura {
    LocalDate fechaEntrega;
    Servicio[] servicios;

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Servicio[] getServicios() {
        return servicios;
    }

    public void setServicios(Servicio[] servicios) {
        this.servicios = servicios;
    }

    public double getCosto() {
        double costo = 0.0;
        for (Servicio servicio: servicios){
            costo += servicio.getCosto();
        }
        return costo;
    }

    public static class Servicio {
        String descripcion;
        Double costo;

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public Double getCosto() {
            return costo;
        }

        public void setCosto(Double costo) {
            this.costo = costo;
        }
    }
}
