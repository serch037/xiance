/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author vel
 */
public class HistorialNota {
//  Variables para realizar busquedas
    private String criterio;
    private String inicio;
    private String fin;
    private String buscado;
    private String busqueda;
    private String entregar;
    private int idOrden;
    private double pago;
//  Variables para mostrar datos
    
    
    public HistorialNota() {
        this.criterio = "pendiente";
        this.inicio = "" + new java.sql.Timestamp(new java.util.Date().getTime());
        this.fin = null;
        this.buscado = null;
        this.busqueda = null;
        this.entregar = "ninguno";
        this.idOrden = -1;
        this.pago = -1.0;
    }
    /**
     * @return the criterio
     */
    public String getCriterio() {
        return criterio;
    }

    /**
     * @param criterio the criterio to set
     */
    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    /**
     * @return the inicio
     */
    public String getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fin
     */
    public String getFin() {
        return fin;
    }

    /**
     * @param fin the fin to set
     */
    public void setFin(String fin) {
        this.fin = fin;
    }

    /**
     * @return the buscado
     */
    public String getBuscado() {
        return buscado;
    }

    /**
     * @param buscado the buscado to set
     */
    public void setBuscado(String buscado) {
        this.buscado = buscado;
    }

    /**
     * @return the busqueda
     */
    public String getBusqueda() {
        return busqueda;
    }

    /**
     * @param busqueda the busqueda to set
     */
    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    /**
     * @return the entregar
     */
    public String getEntregar() {
        return entregar;
    }

    /**
     * @param entregar the entregar to set
     */
    public void setEntregar(String entregar) {
        this.entregar = entregar;
    }

    /**
     * @return the idOrden
     */
    public int getIdOrden() {
        return idOrden;
    }

    /**
     * @param idOrden the idOrden to set
     */
    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    /**
     * @return the pago
     */
    public double getPago() {
        return pago;
    }

    /**
     * @param pago the pago to set
     */
    public void setPago(double pago) {
        this.pago = pago;
    }
}
