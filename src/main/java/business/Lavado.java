package business;

import java.time.LocalDate;

public class Lavado {
    final double COSTO_POR_KILO = 25;
    final double COSTO_INICIAL = 30;
    final double PORCENTAJE_URGENCIA = .05;

    int id;
    int lavadora;
    double kilos;
    boolean urgencia;
    LocalDate fechaEntrega;
    int idOrden;
    PrendaLavado[] lavadosEspeciales;

    public int getLavadora() {
        return lavadora;
    }

    public void setLavadora(int lavadora) {
        this.lavadora = lavadora;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getKilos() {
        return kilos;
    }

    public void setKilos(double kilos) {
        this.kilos = kilos;
    }

    public boolean isUrgencia() {
        return urgencia;
    }

    public void setUrgencia(boolean urgencia) {
        this.urgencia = urgencia;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public double getCosto() {
        double costo = 0;
        if (kilos < 3){
            costo = COSTO_POR_KILO;
        }
        if (kilos > 3) {
            costo += COSTO_POR_KILO * kilos;
        }
        if (urgencia) {
            costo += costo * PORCENTAJE_URGENCIA;
        }
        if (lavadosEspeciales.length > 0) {
            for (PrendaLavado prenda : lavadosEspeciales) {
                costo+= prenda.getCosto();
            }
        }
        return costo;
    }


    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public PrendaLavado[] getLavadosEspeciales() {
        return lavadosEspeciales;
    }

    public void setLavadosEspeciales(PrendaLavado[] lavadosEspeciales) {
        this.lavadosEspeciales = lavadosEspeciales;
    }
    public void setLavadosEspecialesClienteID(int clienteID){
        for (PrendaLavado lavado: lavadosEspeciales) {
            lavado.setIdCliente(clienteID);
        }
    }
}

