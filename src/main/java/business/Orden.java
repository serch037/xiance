package business;

import java.time.LocalDate;

public class Orden {
    int id;
    Cliente cliente;
    LocalDate fecha;
    Planchado planchado;
    Lavado lavado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCostoTotal() {
        return 0.0;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente Cliente) {
        this.cliente = cliente;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Planchado getPlanchado() {
        return planchado;
    }

    public void setPlanchado(Planchado planchado) {
        this.planchado = planchado;
    }

    public Lavado getLavado() {
        return lavado;
    }

    public void setLavado(Lavado lavado) {
        this.lavado = lavado;
    }
}
