package business;

import java.time.LocalDate;
import java.util.ArrayList;

public class Planchado {
    final double NORMAL = 40;
    final double DELICADA = 40;
    final double GANCHO = 40;
    final double URGENTE = .10;
    boolean gancho;
    LocalDate fechaEntrega;
    ArrayList<Detalle> detalles;

    public Planchado() {
        detalles = new ArrayList<>();
    }

    public void addDetalle(Detalle detalle) {
        if (detalles == null) {
            detalles = new ArrayList<>();
        }
        detalles.add(detalle);
    }


    public boolean isGancho() {
        return gancho;
    }

    public void setGancho(boolean gancho) {
        this.gancho = gancho;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public ArrayList<Detalle> getDetalles() {
        return detalles;
    }

    public double getCosto() {
        double costo = 0.0;
        if (!detalles.isEmpty()) {
            for (Detalle detalle : detalles) {
                if (gancho) {
                    costo += GANCHO;
                }
                if (detalle.isDelicada()) {
                    costo += DELICADA;
                }
                if (detalle.isLarga()) {
                    costo += NORMAL * 2;
                }
                if (detalle.isUrgente()) {
                    costo += costo * URGENTE;
                }
            }
        }

        return costo;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public static class Detalle {
        double cantidad;
        boolean larga;
        boolean delicada;
        boolean urgente;

        public double getCantidad() {
            return cantidad;
        }

        public void setCantidad(double cantidad) {
            this.cantidad = cantidad;
        }

        public boolean isLarga() {
            return larga;
        }

        public void setLarga(boolean larga) {
            this.larga = larga;
        }

        public boolean isDelicada() {
            return delicada;
        }

        public void setDelicada(boolean delicada) {
            this.delicada = delicada;
        }

        public boolean isUrgente() {
            return urgente;
        }

        public void setUrgente(boolean urgente) {
            this.urgente = urgente;
        }
    }
}
