package business;

import java.time.LocalDate;

public class Tintoreria {
    LocalDate fechaEntrega;
    Prenda[] prendas;
    final double PRECIO_URGENTE = .10;

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Prenda[] getPrendas() {
        return prendas;
    }

    public void setPrendas(Prenda[] prendas) {
        this.prendas = prendas;
    }

    public double getCosto(){
        double costo = 0.0;
        for (Prenda prenda : prendas){
            if (prenda.urgente){
                costo += prenda.costo + prenda.costo*PRECIO_URGENTE;
            } else {
                costo += prenda.costo;
            }
        }
        return  costo;
    }

    public static class Prenda {
        Double cantidad;
        Boolean urgente;
        String tipo;
        String tipoMaquila;
        String descripcion;
        Double costo;

        public Double getCantidad() {
            return cantidad;
        }

        public void setCantidad(Double cantidad) {
            this.cantidad = cantidad;
        }

        public Boolean getUrgente() {
            return urgente;
        }

        public void setUrgente(Boolean urgente) {
            this.urgente = urgente;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public String getTipoMaquila() {
            return tipoMaquila;
        }

        public void setTipoMaquila(String tipoMaquila) {
            this.tipoMaquila = tipoMaquila;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public Double getCosto() {
            return costo;
        }

        public void setCosto(Double costo) {
            this.costo = costo;
        }
    }
}
