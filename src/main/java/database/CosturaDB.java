package database;

import business.Costura;

import java.sql.*;

public class CosturaDB {
    public static void addServicios(int idCostura, Costura.Servicio[] servicios) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            for (Costura.Servicio servicio : servicios) {
                String sql = "INSERT INTO costura_servicio" +
                        "(idCostura, descripcion, costo)" +
                        "VALUES (?,?,?)";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, idCostura);
                ps.setString(2, servicio.getDescripcion());
                ps.setDouble(3, servicio.getCosto());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            pool.freeConnection(connection);
        }
    }
}

