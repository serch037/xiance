package database;

import business.Lavado;
import business.PrendaLavado;

import java.sql.*;
import java.util.ArrayList;

public class LavadoDB {
    public static void addLavadosEspeciales(int lavadoID, PrendaLavado[] lavados) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            for (PrendaLavado prenda : lavados) {
                String sql = "INSERT INTO prendaespecial" +
                        "(descripcion,tipo,idCliente)" +
                        "VALUES (?,?,?)";
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1,prenda.getDescripcion());
                ps.setString(2,prenda.getTipo());
                ps.setInt(3,prenda.getIdCliente());
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                if (rs.next()){
                    System.out.println("NEXT");
                    int idPrenda = rs.getInt(1);
                    sql = "INSERT INTO lavado_prendaesp" +
                            "(idLavado, idPrendaEspecial, costoPrendaEspecial)" +
                            "VALUES (?,?,?)";
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1,lavadoID);
                    ps.setInt(2, idPrenda);
                    ps.setDouble(3, prenda.getCosto());
                    ps.executeUpdate();
                }
            }

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            pool.freeConnection(connection);
        }
    }
}
