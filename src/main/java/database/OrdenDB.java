package database;

import business.*;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class OrdenDB {
    public static HashMap<String, Integer> fillOrden(int idCliente, Lavado lavado, Planchado planchado, Tintoreria tintoreria, Costura costura) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        CallableStatement cs = null;
        ResultSet rs = null;
        try {
            String sql = "{call guardarOrden(" +
                    "?," +
                    "?," +
                    "?," +
                    "?, ?,?,?," +
                    "?, ?, ?," +
                    "?,?," +
                    "?,?," +
                    "?,?,?,?,?)}";
            cs = connection.prepareCall(sql);
            cs.setInt(1, idCliente);
            cs.setDouble(2, lavado.getCosto());
            cs.setString(3, "1");
            cs.setDouble(4, lavado.getCosto());
            cs.setDate(5, Date.valueOf(lavado.getFechaEntrega()));
            cs.setDouble(6, lavado.getKilos());
            cs.setBoolean(7, lavado.isUrgencia());
            cs.setDouble(8, planchado.getCosto());
            cs.setDate(9, Date.valueOf(planchado.getFechaEntrega()));
            cs.setBoolean(10, planchado.isGancho());
            cs.setDouble(11, tintoreria.getCosto());
            cs.setDate(12, Date.valueOf(tintoreria.getFechaEntrega()));
            cs.setDouble(13, costura.getCosto());
            cs.setDate(14, Date.valueOf(costura.getFechaEntrega()));
            cs.registerOutParameter(15, Types.INTEGER);
            cs.registerOutParameter(16, Types.INTEGER);
            cs.registerOutParameter(17, Types.INTEGER);
            cs.registerOutParameter(18, Types.INTEGER);
            cs.registerOutParameter(19, Types.INTEGER);
            boolean hasResults = cs.execute();
            while (hasResults) {
                rs = cs.getResultSet();
                hasResults = cs.getMoreResults();
            }
            HashMap<String, Integer> output = new HashMap<>();
            output.put("idOrden", cs.getInt(15));
            output.put("idLavado", cs.getInt(16));
            output.put("idPlanchado", cs.getInt(17));
            output.put("idTintoreria", cs.getInt(18));
            output.put("idCostura", cs.getInt(19));
            System.out.println(output);
            return output;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closeCallableStatement(cs);
            pool.freeConnection(connection);
        }
    }

    public static ArrayList<Orden> findLavadosIncompletos() {
        ArrayList<Orden> ordenes = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT idOrden, TableA.nombre, TableA.apellidos, lavadora, ordenLavado" +
                    "FROM (" +
                    "SELECT *" +
                    "FROM Orden" +
                    "JOIN Cliente" +
                    "ON Orden.idCliente = Cliente.idCliente" +
                    ")AS TableA" +
                    "JOIN Lavado" +
                    "ON TableA.idOrden = Lavado.idOrden" +
                    "WHERE terminado = 0" +
                    "ORDER BY ordenLavado DESC;";
            ps = connection.prepareCall(sql);
            rs = ps.executeQuery();
            Orden orden;
            Cliente cliente;
            Lavado lavado;
            while (rs.next()) {
                cliente = new Cliente();
                lavado = new Lavado();
                orden = new Orden();
                cliente.setNombre(rs.getString("TableA.nombre"));
                cliente.setApellidos(rs.getString("TableA.apellidos"));
                lavado.setLavadora(rs.getInt("lavadora"));
                lavado.setId(rs.getInt("ordenLavado"));
                orden.setId(rs.getInt("idOrden"));
                orden.setCliente(cliente);
                orden.setLavado(lavado);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        return ordenes;
    }
}
