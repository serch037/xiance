package database;

import business.Planchado;
import business.PrendaLavado;

import java.sql.*;
import java.util.ArrayList;

public class PlanchadoDB {
    public static void addDetalles(int idPlanchado, ArrayList<Planchado.Detalle> detalles) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            for (Planchado.Detalle detalle : detalles) {
                String sql = "INSERT INTO detalleplanchado" +
                        "(cantidad, larga, delicada,urgente)" +
                        "VALUES (?,?,?,?)";
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setDouble(1, detalle.getCantidad());
                ps.setBoolean(2, detalle.isLarga());
                ps.setBoolean(3, detalle.isDelicada());
                ps.setBoolean(4, detalle.isUrgente());
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    System.out.println("NEXT");
                    int idDetalle = rs.getInt(1);
                    sql = "INSERT INTO planchado_detalleplanchado" +
                            "(idDetallePlanchado, idPlanchado)" +
                            "VALUES (?,?)";
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1, idDetalle);
                    ps.setInt(2, idPlanchado);
                    ps.executeUpdate();
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            pool.freeConnection(connection);
        }
    }
}
