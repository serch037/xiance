package database;

import business.Tintoreria;

import java.sql.*;

public class TintoreriaDB {
    public static void addPrendas(int idCliente, int idTintoreria, Tintoreria.Prenda[] prendas) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            for (Tintoreria.Prenda prenda : prendas) {
                String sql = "INSERT INTO prendatintoreria" +
                        "(descripcion, tipo, tipo_maq, urgencia, idCliente)" +
                        "VALUES (?,?,?,?,?)";
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, prenda.getDescripcion());
                ps.setString(2, prenda.getTipo());
                ps.setString(3, prenda.getTipoMaquila());
                ps.setBoolean(4, prenda.getUrgente());
                ps.setInt(5, idCliente);
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    System.out.println("NEXT");
                    int idPrenda = rs.getInt(1);
                    sql = "INSERT INTO tint_prendatint" +
                            "(idTintoreria, idPrendaTintoreria, costoPrendaTintoreria)" +
                            "VALUES (?,?,?)";
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1, idTintoreria);
                    ps.setInt(2, idPrenda);
                    ps.setDouble(3, prenda.getCosto());
                    ps.executeUpdate();
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            pool.freeConnection(connection);
        }
    }
}
