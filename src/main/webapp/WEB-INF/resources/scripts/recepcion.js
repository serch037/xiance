// Variables
// Lavado
let lavadosEspeciales = [];
let lavadoCount = 0
// Tintorería
let prendasTintoreria = [];
let tintoreriaCount = 0;
// Costura
let costura = [];
let costuraCount = 0;

let data = {}

//lavadoEspecial
function agregarLavadoEspecial(event) {
    let tipo = $("#tipo_lavado option:selected").text()
    let costo = $("input[name=especial_cost]").val()
    let descripcion = $("input[name=lavado_descripcion]").val()
    let html = `<div class="row" lavadoId=${lavadoCount}>
                                        <div class="col-md-1"><button class="btn btn-default" onclick="quitarLavadoEspecial(this)">-</div>
                                        <div class="col-md-11">${tipo} ${descripcion} $${costo}</div>
                                    </div>`
    $("#lav_especiales_agregar").append(html)
    console.log(tipo);
    lavadosEspeciales.push({id: lavadoCount, tipo: tipo, costo: costo, descripcion: descripcion})
    lavadoCount++
    console.log(lavadosEspeciales)
};

function quitarLavadoEspecial(event) {
    console.log(event)
    const parent = $(event).parent('div').parent('div')
    const id = parent.attr('lavadoId')
    parent.remove()
    lavadosEspeciales = filter(lavadosEspeciales, id)
    console.log(lavadosEspeciales)
};

function agregarPrendaTintoreria(event) {
    let tipo = $("#tipo_tintoreria option:selected").text()
    let tipo_maquila = $("#tinto_tipo_maquila option:selected").text()
    let descripcion = $("input[name=tintoreria_descripcion]").val()
    let precio = $("input[name=tintoreria_cost]").val()
    let html = `<div class="row" tintoreriaId=${tintoreriaCount}>
                                        <div class="col-md-2"><button class="btn" onclick="quitarPrendaTintoreria(${tintoreriaCount})"></button></div>
                                        <div class="col-md-2"><input type="number" min="0" name="tintoreria_cantidad" id="tintocantidad${tintoreriaCount}"
                                                                     class="form-control"
                                                                     aria-label="Cantidad Tintoreria"
                                                                     onchange="actualizarTintoCantidad(${tintoreriaCount})"></div>
                                        <div class="col-md-2"><input class="form-check-input" type="checkbox" id="tintourgencia${tintoreriaCount}"
                                                                     name="tintoreria_urgente" class="form-control"
                                                                     aria-label="tintoreria urgente" onchange="actualizarTintoUrgencia(${tintoreriaCount})"></div>
                                        <div class="col-md-2">${tipo} ${descripcion}</div>
                                        <div class="col-md-2">$${precio}</div>
                                        <div class="col-md-2">Tipo ${tipo_maquila}</div>
                                    </div>`
    $("#tinto_especial_agregar").append(html)
    prendasTintoreria.push({
        id: tintoreriaCount,
        cantidad: 0,
        urgente: false,
        tipo: tipo,
        tipoMaquila: tipo_maquila,
        descripcion: descripcion,
        costo: precio
    })
    tintoreriaCount++
    console.log(prendasTintoreria)
};

function quitarPrendaTintoreria(id) {
    console.log(id);
    const parent = $(`.row[tintoreriaID=${id}]`)
    parent.remove()
    prendasTintoreria = filter(prendasTintoreria, id)
};

function actualizarTintoCantidad(id) {
    const prenda = prendasTintoreria.find(e => e.id == id)
    prenda.cantidad = $('#tintocantidad' + id).val()
}

function actualizarTintoUrgencia(id) {
    const prenda = prendasTintoreria.find(e => e.id == id)
    prenda.urgente = $('#tintourgencia' + id).is(":checked")
}

function agregarCostura() {
    let servicio = $("input[name=costura_servicio]").val()
    let html = `
    <div class="row" costuraID=${costuraCount}>
                                         <div class="col-md-4"><button class="btn" onclick="quitarCostura(${costuraCount})">-</button></div>
                                         <div class="col-md-4">${servicio}</div>
                                         <div class="col-md-4"><input type="number" min="0" name="costura_cost"
                                                                      class="form-control" placeholder="Costo"
                                                                      id = "costoCostura${costuraCount}"
                                                                      onchange="actualizarCostoCostura(${costuraCount})"
                                                                      aria-label="Costo costura"></div>
                                     </div>
    `
    $('#costura_agregar').append(html)
    costura.push({
        id: costuraCount,
        descripcion: servicio,
        costo: 0
    })
    costuraCount++
}

function actualizarCostoCostura(id) {
    const servicio = costura.find(e => e.id == id)
    console.log(servicio)
    console.log($('#costoCostura' + id).val())
    servicio.costo = $('#costoCostura' + id).val()
    console.log(servicio)
}

function quitarCostura(id) {
    const parent = $(`.row[costuraID=${id}]`)
    parent.remove()
    costura = filter(costura, id)
}

function postOrder() {
    console.log("Sending post")
    console.log(data)
    $.post('fillOrden', $.param(data), function (response) {
        console.log(response)
    })
};

function generarTabla() {
    function getRowHtml(servicio, cantidad, concepto, entrega, costo) {
        return `<tr>
                          <th scope="row">${servicio}</th>
                          <td>${cantidad}</td>
                          <td>${concepto}</td>
                          <td>${entrega}</td>
                          <td>${costo}</td>
                        </tr>`
    }

    data = {
        // Lavados
        action: "fillOrden",
        idCliente: "0",
        kilosLavado: $("input[name=lavado_kilos]").val(),
        lavadosEspeciales: JSON.stringify(lavadosEspeciales),
        urgenciaLavado: $('input[name=lavado_urgente]').is(":checked"),
        //Planchado
        plaNorNor: $("input[name=plaNorNor]").val(),
        plaNorUrg: $("input[name=plaNorUrg]").val(),
        plaNorDel: $("input[name=plaNorDel]").val(),
        plaNorDelUrg: $("input[name=plaNorDelUrg]").val(),
        plaLaNor: $("input[name=plaLaNor]").val(),
        plaLaUrg: $("input[name=plaLaUrg]").val(),
        plaLaDel: $("input[name=plaLaDel]").val(),
        plaLaDelUrg: $("input[name=plaLaDelUrg]").val(),
        plaGancho: $('input[name=planchado_congancho]').is(":checked"),
        //Tintorería
        prendasTintoreria: JSON.stringify(prendasTintoreria),
        //Costura
        costura: JSON.stringify(costura)
    }

    let html = ""
    //Lavado
    if (data.kilosLavado > 0) {
        html = getRowHtml("Lavado", data.kilosLavado, "Kg. de ropa lavada", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    JSON.parse(data.lavadosEspeciales).forEach(e => {
        html = getRowHtml("Lavado", 1, e.tipo + " " + e.descripcion, "fecha", e.costo)
        $("#tablaResumen").append(html)
    })

    //Planchado
    if (data.plaNorNor > 0) {
        html = getRowHtml("Planchado", data.plaNorNor, "Prenda normal planchado normal", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaNorDel > 0) {
        html = getRowHtml("Planchado", data.plaNorDel, "Prenda normal planchado delicado", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaNorUrg > 0) {
        html = getRowHtml("Planchado", data.plaNorUrg, "Prenda normal planchado normal (urgente)", "fecha", "costo")
        $("#tablaResumen").append(html)

    }
    if (data.plaNorDelUrg > 0) {
        html = getRowHtml("Planchado", data.plaNorDelUrg, "Prenda normal planchado delicado (urgente)", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaLaNor > 0) {
        html = getRowHtml("Planchado", data.plaLaNor, "Prenda larga planchado normal", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaLaDel > 0) {
        html = getRowHtml("Planchado", data.plaLaDel, "Prenda larga planchado delicado", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaLaUrg > 0) {
        html = getRowHtml("Planchado", data.plaLaUrg, "Prenda larga planchado normal (urgente)", "fecha", "costo")
        $("#tablaResumen").append(html)
    }
    if (data.plaLaDelUrg > 0) {
        html = getRowHtml("Planchado", data.plaLaDelUrg, "Prenda larga planchado delicado (urgente)", "fecha", "costo")
        $("#tablaResumen").append(html)
    }

    //Tintorería
    JSON.parse(data.prendasTintoreria).forEach(e => {
        html = getRowHtml("Tintoreria", e.cantidad, e.tipo + " " + e.descripcion, "fecha", e.costo)
        $("#tablaResumen").append(html)
    })

    //Costura
    JSON.parse(data.costura).forEach(e => {
        html = getRowHtml("Costura", 1, e.descripcion, "fecha", e.costo)
        $("#tablaResumen").append(html)
    })
}


    function filter(array, index) {
        return array.filter(e => e.id !== index)
    };
