<%@tag description="Main Wrapper Tag" pageEncoding="UTF-8"%>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
    <body>
    <div class="row">
    <div class="col">
    	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
		  <a class="navbar-brand" href="index.jsp">Xiance</a>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li>
		        <a class="nav-link" href="cliente.jsp">Generar Orden <span class="sr-only">(current)</span></a>
		      </li>
		      <li><a class="nav-link" href="/secured/lavado.jsp">Lavado</a></li>
			  <li><a class="nav-link" href="/secured/planchado.jsp">Planchado</a></li>
			  <li><a class="nav-link" href="/secured/costura.jsp">Costura</a></li>
			  <li><a class="nav-link" href="/secured/maquila.jsp">Maquila</a></li>
			  <li><a class="nav-link" href="/secured/historialnota.jsp">Historial de notas</a></li>
		      
		    </ul>
		  </div>
		</nav>
     </div>
     </div>
        <jsp:doBody/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		
    </body>
</html>
