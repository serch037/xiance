<%-- 
    Document   : index
    Created on : Nov 23, 2017, 8:05:30 AM
    Author     : vel
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="historial" scope="session" class="business.HistorialNota" />
        <a href="secured/historialnota.jsp">Historial de notas</a>
    </body>
</html>
