<%--
  Created by IntelliJ IDEA.
  User: sumtr
  Date: 11/16/2017
  Time: 10:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="container">
        <div class="jumbotron">
            <div class="row">
                <div class="col-lg-6">
                    <input type="hidden" name="action" value="findClients">
                    <input type="text" name="nomCliente" class="form-control" placeholder="Buscar Cliente"
                           aria-label="Buscar Cliente">
                           <br>
                    <input type="button" value="Buscar" onclick="findClient()">
                    </span>
                </div>
            </div>
            <p></p>
            <div class="row">
                <div class="col-lg-2 col-md-5 col-xs-4">
                    <input type="text" class="form-control" placeholder="Nombre" id="nomCliente"
                           onchange="setModified()">
                </div>
                <div class="col-lg-2 col-md-5 col-xs-4">
                    <input type="text" class="form-control" placeholder="Apellido" id="apCliente"
                           onchange="setModified()">
                </div>
                <div class="col-lg-2 col-md-5 col-xs-4">
                    <input type="text" class="form-control" placeholder="Teléfono" id="telCliente">
                </div>
            </div>
            <p></p>
            <div class="row">
                <div class="col">
                    <div class="form-group col-md-10 col-md-offset-1">
                        <label for="clientSelect"></label>
                        <select multiple class="form-control" id="clientSelect"
                                onchange="chooseclient(this)" style="display: none">
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <button class="btn btn-secondary btn-block" type="button" onclick="newClient()">Vaciar Campos</button>
            </div>
            <p></p>
            <div class="row">
                <button class="btn btn-secondary btn-block" type="button" onclick="postClient()">Crear orden</button>
            </div>
        </div>
    </div>
    <script>
        'use strict';
        let cliente = 0;
        let modified = false;

        function setModified() {
            modified = true;
        }

        function newClient() {
            $("#clientSelect").val([]);
            $("#nomCliente").val('');
            $("#telCliente").val('');
            $("#apCliente").val('');
            $('#clientSelect').hide();
            $("label[for='clientSelect']").text("Crear nuevo registro");
            $('#clientSelect').empty();
        }

        function chooseclient(a) {
            let selected = document.getElementById("clientSelect");
            let cliente = selected.value;
            let optionText = selected.options[selected.selectedIndex].text;
            optionText = optionText.split(" ");
            document.getElementById("nomCliente").value = optionText[0];
            document.getElementById("telCliente").value = optionText[2];
            modified = false
        }

        function postClient() {
            console.log("PostClient")
            let nomCliente = document.getElementById("nomCliente").value
            let telCliente = document.getElementById("telCliente").value
            let apCliente = document.getElementById("apCliente").value
            let data = {
                action: "selectClient",
                idCliente: $('#clientSelect').val()[0],
                nomCliente: nomCliente,
                telCliente: telCliente,
                apCliente: apCliente,
                modified: modified
            }
            $.post('fillOrden', $.param(data), function (response) {
                window.location.href = response;
            })
        }

        function findClient() {
            console.log("FindClient")
            const data = {
                action: "findClients",
                nomCliente: $("input[name='nomCliente']").val()
            }
            $.get('fillOrden', $.param(data), function (responseText) {
                let content = ''
                Object.keys(responseText).forEach(function (k) {
                    console.log(responseText[k])
                    content += '<option value="' + responseText[k]['id'] + '">' + responseText[k]['nombre'] + " "
                        + responseText[k]['apellidos'] + " " + responseText[k]['telefono'] + '</option>';
                    console.log(content)
                })
                if ($.isEmptyObject(responseText)) {
                    $('#clientSelect').hide()
                    $("label[for='clientSelect']").text("No se encontraron resultados")
                } else {
                    $('#clientSelect').show()
                    $("label[for='clientSelect']").text("Resultados de la búsqueda")
                }

                $('#clientSelect').empty().append(content)
            })
        }
    </script>
</t:wrapper>
