<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
	<div class="container">
	  <div class="jumbotron">
		  <h1 align="center">Lista de Costura</h1>
		  <div class="row">
		  	<div class="col">
	  		  <table class="table table-striped">
		  			<thead>
		  				<tr>
		  					<th>Terimnado</th>
		  					<th>Nota</th>
		  					<th>Cliente</th>
		  					<th>Descripción</th>
		  				</tr>
		  			</thead>
		  			<tbody>
		  				<tr>
		  					<td><input type="checkbox" name="terminado"></td>
		  					<td>001</td>
		  					<td>Juan</td>
	  					  <td>Cambiar Botón</td>
		  				</tr>
		  			</tbody>
		  		</table>
		  	</div>
		  </div>
		  <div class="row">
		  	<div class="col" align="center">
		  		<input type="submit" value="Guardar Cambios" class="btn">
		  	</div>
		  </div>
	  </div>
    </div>


    <script>

        <c:import url="/WEB-INF/resources/scripts/costura.js"/>
    </script>

</t:wrapper>