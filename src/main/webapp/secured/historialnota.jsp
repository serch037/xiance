<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.sql.*, java.util.*, java.io.*" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Historial de Notas</title>
    <link href="/WEB-INF/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://localhost:3306/xiance"
                   user="admin" password="password"/>

<jsp:useBean id="historial" scope="page" class="business.HistorialNota"/>
<jsp:setProperty name="historial" property="inicio"/>
<jsp:setProperty name="historial" property="fin"/>
<jsp:setProperty name="historial" property="buscado"/>
<jsp:setProperty name="historial" property="criterio"/>
<jsp:setProperty name="historial" property="busqueda"/>
<jsp:setProperty name="historial" property="entregar"/>
<jsp:setProperty name="historial" property="idOrden"/>
<jsp:setProperty name="historial" property="pago"/>

<c:choose>
    <c:when test="${historial.busqueda eq 'busqueda'}">
        <c:choose>
            <c:when test="${historial.criterio eq 'rango'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha >= '<c:out
                        value="${historial.inicio}"/>' AND fecha <= '<c:out value="${historial.fin}"/>' AND ( nombre = '<c:out
                        value="${historial.buscado}"/>' OR idOrden = '<c:out
                        value="${historial.buscado}"/>' ) GROUP BY idOrden;
                </sql:query>
            </c:when>
            <c:when test="${historial.criterio eq 'inicial'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha >= '<c:out
                        value="${historial.inicio}"/>' AND ( nombre = '<c:out
                        value="${historial.buscado}"/>' OR idOrden = '<c:out
                        value="${historial.buscado}"/>' ) GROUP BY idOrden;
                </sql:query>
            </c:when>
            <c:when test="${historial.criterio eq 'dia'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha = '<c:out
                        value="${historial.inicio}"/>' AND ( nombre = '<c:out
                        value="${historial.buscado}"/>' OR idOrden = '<c:out
                        value="${historial.buscado}"/>' ) GROUP BY idOrden;
                </sql:query>
            </c:when>
        </c:choose>
    </c:when>
    <c:when test="${historial.busqueda eq 'historial'}">
        <c:choose>
            <c:when test="${historial.criterio eq 'rango'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha >= '<c:out
                        value="${historial.inicio}"/>' AND fecha <= '<c:out
                        value="${historial.fin}"/>' GROUP BY idOrden;
                </sql:query>
            </c:when>
            <c:when test="${historial.criterio eq 'inicial'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha >= '<c:out
                        value="${historial.inicio}"/>' GROUP BY idOrden;
                </sql:query>
            </c:when>
            <c:when test="${historial.criterio eq 'dia'}">
                <sql:query var="result" dataSource="${snapshot}">
                    SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE (entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false) AND fecha = '<c:out
                        value="${historial.inicio}"/>' GROUP BY idOrden;
                </sql:query>
            </c:when>
        </c:choose>
    </c:when>
    <c:otherwise>
        <sql:query var="result" dataSource="${snapshot}">
            SELECT idOrden, nombre, SUM(cantidad) AS adelanto, costo_total, entregaLavado, entregaPlanchado, entregaTintoreria, entregaCostura FROM orden NATURAL JOIN cliente NATURAL JOIN pago WHERE entregaLavado = false OR entregaPlanchado = false OR entregaTintoreria = false OR entregaCostura = false GROUP BY idOrden;
        </sql:query>
    </c:otherwise>
</c:choose>

<div class="container">
    <p></p><br>
</div>

<div class="container">
    <div class="jumbotron">
        <h1 align="center">Historial de Notas</h1>
        <!-- EMPIEZA FORM -->
        <form id="historialNota" name="historialNota" action="busquedanota.jsp" method="POST">
            <!-- Input Fecha Inicial -->
            <div class="row">
                <div class="col-sm-3 col-md-offset-1">
                    <h4 class="col">Fecha Inicial</h4>
                </div>
                <div class="col col-sm-3 col-xs-6">
                    <input id="inicio" type="date" name="inicio" class="form-control">
                </div>
                <div class="col col-sm-4 col-xs-5">
                    <input type="text" id="barra" name="buscado" class="form-control"
                           placeholder="Cliente o número de nota">
                </div>
            </div>
            <br>
            <!-- Input Fecha Final -->
            <div class="row">
                <div class="col-sm-3 col-md-offset-1">
                    <h4 class="col">Fecha Final</h4>
                </div>
                <div class="col col-sm-3 col-xs-6">
                    <input id="fin" type="date" name="fin" class="form-control">
                </div>
            </div>
            <br>
            <!-- Creiterios de Busqueda -->
            <div class="row">
                <div class="col col-md-3 col-md-offset-1">
                    <label><h5><input type="radio" id="rango" name="criterio" value="rango"> Buscar entre la fecha
                        inicial y final</h5></label>
                </div>
                <div class="col col-md-offset-0 col-md-3">
                    <label><h5><input type="radio" id="inicial" name="criterio" value="inicial"/> Buscar a partir de la
                        fecha inicial</h5></label>
                </div>
                <div class="col col-md-offset-0 col-md-3">
                    <label><h5><input type="radio" id="dia" name="criterio" value="dia"/> Buscar en el día de la fecha
                        inicial</h5></label>
                </div>
            </div>
            <br>
            <!-- Botones  -->
            <div class="row">
                <div class="col col-md-3 col-md-offset-1">
                    <button type="submit" form="historialNota" value="busqueda" name="busqueda" class="btn">Realizar
                        Busqueda
                    </button>
                </div>
                <div class="col col-md-3">
                    <button type="submit" form="historialNota" value="historial" name="busqueda" class="btn">Mostrar
                        Historial
                    </button>
                </div>
                <div class="col col-md-3">
                    <button type="submit" form="historialNota" value="pendiente" name="busqueda" class="btn">Notas
                        Pendientes
                    </button>
                </div>
            </div>
            <br>
        </form>
        <!-- EMPIEZA FORM -->
        <form name="actualizarNota" id="actualizarNota" action="busquedanota.jsp" method="POST">

            <!-- Resultados de la tabla -->
            <input type="hidden" name="inicio" value='<c:out value="${historial.buscado}"/>'/>
            <input type="hidden" name="fin" value='<c:out value="${historial.fin}"/>'/>
            <input type="hidden" name="buscado" value='<c:out value="${historial.buscado}"/>'/>
            <input type="hidden" name="criterio" value='<c:out value="${historial.criterio}"/>'/>
            <input type="hidden" name="busqueda" value='<c:out value="${historial.busqueda}"/>'/>
            <input type="hidden" id="entregar" name="entregar" value="ninguno"/>
            <input type="hidden" id="idOrden" name="idOrden" value="ninguno"/>
            <input type="hidden" id="pago" name="pago" value="0.0"/>

            <!-- Empieza Tabla -->
            <div class="row">
                <div class="col">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th rowspan="2">Pagar</th>
                            <th rowspan="2">Nota</th>
                            <th rowspan="2">Cliente</th>
                            <th rowspan="2">Adelanto</th>
                            <th rowspan="2">Total</th>
                            <th colspan="4">Ropa entregada</th>
                        </tr>
                        <tr>
                            <th>Lavado</th>
                            <th>Planchado</th>
                            <th>Tintorería</th>
                            <th>Costura</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="row" items="${result.rows}">
                            <tr>
                                <td>
                                    <c:choose>
                                        <c:when test="${row.costo_total eq row.adelanto}">
                                            <p>Pagado</p>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button"
                                                    onClick="pagarNota(<c:out value="${row.idOrden}"/>, '<c:out
                                                            value="${row.costo_total - row.adelanto}"/>')">Pagar
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td><c:out value="${row.idOrden}"/></td>
                                <td><c:out value="${row.nombre}"/></td>
                                <td><c:out value="${row.adelanto}"/></td>
                                <td><c:out value="${row.costo_total}"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${row.entregaLavado}">
                                            <p>Entregado</p>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" onClick='enviarNota(<c:out
                                                    value="${row.idOrden}"/>, "entregaLavado")'>Entregar
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${row.entregaPlanchado}">
                                            <p>Entregado</p>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" onClick='enviarNota(<c:out
                                                    value="${row.idOrden}"/>, "entregaPlanchado")'>Entregar
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${row.entregaTintoreria}">
                                            <p>Entregado</p>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" onClick='enviarNota(<c:out
                                                    value="${row.idOrden}"/>, "entregaTintoreria")'>Entregar
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${row.entregaCostura}">
                                            <p>Entregado</p>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" onClick='enviarNota(<c:out
                                                    value="${row.idOrden}"/>, "entregaCostura")'>Entregar
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <!-- Termina Tabla -->
                </div>
            </div>
        </form>

        <script>

            (function () {
                document.getElementById("barra").value = '<c:out value="${historial.buscado}"/>';
                document.getElementById("inicio").value = '<c:out value="${historial.inicio}"/>';
                document.getElementById("fin").value = '<c:out value="${historial.fin}"/>';
                document.getElementById('<c:out value="${historial.criterio}"/>').checked = true;
            })();

            function pagarNota(id, pago) {
                document.getElementById("idOrden").value = id;
                document.getElementById("pago").value = pago;
                document.getElementById("entregar").value = "pago";
                document.getElementById("actualizarNota").submit();
            }

            function enviarNota(id, campo) {
                var campoIdOrden = document.getElementById("idOrden");
                var campoEntregar = document.getElementById("entregar");
                campoIdOrden.value = id;
                campoEntregar.value = campo;
                document.getElementById("actualizarNota").submit();
            }
        </script>
</body>
</html>
