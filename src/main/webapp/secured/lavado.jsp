<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
	<div class="container">
	  <div class="jumbotron">
		  <h1 align="center">Lavado</h1>
		  <%--<div class="row">
			  <div class="col">
				<form method="get" action="./getUser">
					<div class="col col-xs-10 col-md-10">
						<div class="row">
							<input type="text" name="user" class="form-control" placeholder="Buscar Cliente" aria-label="Buscar Cliente">
						</div>
					</div>
					<div class="col col-md-1 col-xs-1 col-xs-offset-1 col-md-offset-1">
						<div class="row">
							<input type="submit" value="Buscar" class="btn btn-secondary">
						</div>
					</div>
				</form>
			  </div>
		  </div>--%>
		  <br>
		  <div class="row">
		  	<div class="col col-md-offset-0 col-md-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Terminado</th>
							<th>Nota</th>
							<th>Cliente</th>
							<th>Núm. Lavadora</th>
							<th>Orden</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="checkbox" name="terminado"></td>
							<td>001</td>
							<td>Juan</td>
							<td>00</td>
							<td>0000</td>
						</tr>
					</tbody>
				</table>
		  	</div>
		  </div>
		  <br>
		  <div class="row">
		  	<div class="col" align="center">
		  		<input type="submit" value="Guardar Cambios" class="btn">
		  	</div>
		  </div>
		  <%--<div class="row">
		  	<div class="col col-md-3">
		  		<input type="text" name="numNota" class="form-control" placeholder="Num. Nota" aria-label="Num. Nota">
		  	</div>
		  	<div class="col col-md-3">
		  		<input type="text" name="numLavadora" class="form-control" placeholder="Lavadora" aria-label="Lavadora">
		  	</div>
		  	<div class="col col-md-3">
		  		<input type="text" name="ordenLavado" class="form-control" placeholder="Orden" aria-label="Orden">
		  	</div>
		  	<div class="col col-md-2 col-md-offset-1">
		  		<input type="submit" value="Agregar" class="btn btn-secondary">
		  	</div>
		  </div>--%>
		  
	  </div>
    </div>





    <script>

        <c:import url="/WEB-INF/resources/scripts/lavado.js"/>
    </script>

</t:wrapper>