<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="container">
        <h1 align="center">&nbsp;</h1>
    </div>
    <div class="container">
        <div class="jumbotron">
            <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                   href="#collapseTwo1">Lavado</a></h4>
                    </div>
                    <div id="collapseTwo1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1">Kilos de ropa para lavado:</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" min="0" max="15" name="lavado_kilos" class="form-control"
                                           aria-label="kilos de lavado">
                                </div>
                                <div class="col-md-3">
                                    Urgente <input class="form-check-input" type="checkbox" name="lavado_urgente"
                                                   class="form-control"
                                                   aria-label="lavado urgente">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h4 class="col-md-12 col-md-offset-0" align="left">Especiales</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-md-3">
                                    <select id="tipo_lavado">
                                        <option value="cobija">Cobija</option>
                                        <option value="edredon">Edredón</option>
                                        <option value="cobertor">Cobertor</option>
                                        <option value="tenis">Tenis</option>
                                        <option value="almohada">Almohada</option>
                                        <option value="cortina">Cortina</option>
                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-3"><input type="number" min="0" name="especial_cost"
                                                                      class="form-control" placeholder="Costo"
                                                                      aria-label="Costo Especial"></div>
                                <div class="col-md-3"><input type="text"  name="lavado_descripcion" class="form-control"
                                           aria-label="kilos de lavado"></div>
                                <div class="col-md-3"><button type="button" value="Agregar" onclick="agregarLavadoEspecial(this)" class="btn btn-primary">Agregar</button></div>
                            </div>

                            <br>

                            <div id="lav_especiales_agregar">
                                    <%--  <div class="row">
                                          <div class="col-md-1"><input type="submit" value="-"></div>
                                          <div class="col-md-11"> Cortina $14</div>
                                      </div>
                                      <br>--%>
                            </div>

                        </div>
                    </div>
                </div>

                    <div class="panel panel-default">
                         <div class="panel-heading">
                             <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                        href="#collapseThree1">Planchado</a></h4>
                         </div>
                         <div id="collapseThree1" class="panel-collapse collapse">
                             <div class="panel-body">
                                 <div class="row">
                                     <div class="col-md-4"></div>
                                     <div class="col-md-2">Normal</div>
                                     <div class="col-md-2">Normal (Urg)</div>
                                     <div class="col-md-2">Delicado</div>
                                     <div class="col-md-2">Delicado(Urg)</div>
                                 </div>
                                 <br>
                                 <div class="row">
                                     <div class="col-md-4">Prendas normales</div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaNorNor"
                                                                  class="form-control"
                                                                  aria-label="cantidad de pnpn"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaNorUrg"
                                                                  class="form-control"
                                                                  aria-label="cantidad de pnpnu"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaNorDel"
                                                                  class="form-control"
                                                                  aria-label="cantidad de pnpd"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaNorDelUrg"
                                                                  class="form-control"
                                                                  aria-label="cantidad de pnpdu"></div>
                                 </div>
                                 <br>
                                 <div class="row">
                                     <div class="col-md-4">Prendas largas</div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaLaNor"
                                                                  class="form-control"
                                                                  aria-label="cantidad de plpn"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaLaUrg"
                                                                  class="form-control"
                                                                  aria-label="cantidad de plpnu"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaLaDel"
                                                                  class="form-control"
                                                                  aria-label="cantidad de plpd"></div>
                                     <div class="col-md-2"><input type="number" min="0" max="30" name="plaLaDelUrg"
                                                                  class="form-control"
                                                                  aria-label="cantidad de plpdu"></div>
                                 </div>
                                 <br>
                                 <div class="row">
                                     <div class="col">
                                         Con gancho <input class="form-check-input" type="checkbox"
                                                           name="planchado_congancho" class="form-control"
                                                           aria-label="plancahdo con gancho">
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="panel panel-default">
                         <div class="panel-heading">
                             <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                        href="#collapseFour1">Tintorería</a></h4>
                         </div>


                         <div id="collapseFour1" class="panel-collapse collapse">
                             <div class="panel-body">
                                  <div class="row">
                                     <div class="col">
                                         <h4 class="col-md-12 col-md-offset-0" align="left">Seleccion de prendas</h4>
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-md-3"><select id="tipo_tintoreria">
                                         <option value="camisa">camisa</option>
                                         <option value="pantalón">pantalón</option>
                                         <option value="saco">saco</option>
                                         <option value="sueter">sueter</option>
                                         <option value="corbata">corbata</option>
                                         <option value="sudadera">sudadera</option>
                                         <option value="chaleco">chaleco</option>
                                         <option value="vestido">vestido</option>
                                         <option value="chamarra">chamarra</option>
                                         <option value="abrigo">abrigo</option>
                                         <option value="gabardina">gabardina</option>
                                         <option value="edredón normal">edredón normal</option>
                                         <option value="edredón plumas">edredón plumas</option>
                                         <option value="vestido de fiesta">vestido de fiesta</option>

                                     </select></div>

                                     <div class="col-md-3"><input type="text"  name="tintoreria_descripcion" class="form-control"
                                           aria-label="descripcion de prenda"></div>

                                     <div class="col-md-1">Tipo</div>
                                     <div class="col-md-1"><select id="tinto_tipo_maquila">
                                         <option value="A">A</option>
                                         <option value="B">B</option>
                                     </select></div>
                                     <div class="col-md-2"><input type="number" min="0" name="tintoreria_cost"
                                                                           class="form-control" placeholder="Costo"
                                                                           aria-label="Costo Especial"></div>
                                     <div class="col-md-2"><button type="button" value="Agregar" onclick="agregarPrendaTintoreria(this)" class="btn btn-primary">Agregar</button></div>
                                 </div>

                                 <div id="tinto_especial_agregar">
                                     <div class="row">
                                         <div class="col-md-2"></div>
                                         <div class="col-md-2">Cantidad</div>
                                         <div class="col-md-2">Urgente</div>
                                         <div class="col-md-6"></div>
                                     </div>

                                     <br>

                                     <%--<div class="row">--%>
                                         <%--<div class="col-md-2"><input type="submit" value="-"></div>--%>
                                         <%--<div class="col-md-2"><input type="number" min="0" name="Tintoreria_cantidad"--%>
                                                                      <%--class="form-control"--%>
                                                                      <%--aria-label="Cantidad Tintoreria"></div>--%>
                                         <%--<div class="col-md-2"><input class="form-check-input" type="checkbox"--%>
                                                                      <%--name="tintoreria_urgente" class="form-control"--%>
                                                                      <%--aria-label="tintoreria urgente"></div>--%>
                                         <%--<div class="col-md-2">Traje gris</div>--%>
                                         <%--<div class="col-md-2">$50</div>--%>
                                         <%--<div class="col-md-2">Tipo A</div>--%>
                                     <%--</div>--%>

                                     <%--<br>--%>

                                 </div>

                                 <br>
                             </div>
                         </div>

                     </div>
                      <div class="panel panel-default">
                         <div class="panel-heading">
                             <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                        href="#collapseFive1">Costura</a></h4>
                         </div>
                         <div id="collapseFive1" class="panel-collapse collapse">
                             <div class="panel-body">
                                 <div class="row">
                                     <div class="col">
                                         <h4 class="col-md-12 col-md-offset-0" align="left">Seleccion de servicios</h4>
                                     </div>
                                 </div>

                                 <div class="row">
                                     <div class="col-md-8"><input type="text" name="costura_servicio"
                                                                           class="form-control"
                                                                           placeholder="Nombre de servicio"
                                                                           aria-label="Servicio Costura"></div>
                                     <div class="col-md-4"><button type="button" value="Agregar" onclick="agregarCostura()" class="btn btn-primary">Agregar</button></div>
                                 </div>

                                 <br>

                                 <div id="costura_agregar">
                                      <%--<div class="row">--%>
                                         <%--<div class="col-md-4"><input type="submit" value="-"></div>--%>
                                         <%--<div class="col-md-4">Servicio A</div>--%>
                                         <%--<div class="col-md-4"><input type="number" min="0" name="costura_cost"--%>
                                                                      <%--class="form-control" placeholder="Costo"--%>
                                                                      <%--aria-label="Costo costura"></div>--%>
                                     <%--</div>--%>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="panel panel-default">
                         <div class="panel-heading">
                             <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                        href="#collapseSix1" onclick="generarTabla()">Orden</a></h4>
                         </div>
                         <div id="collapseSix1" class="panel-collapse collapse">
                             <div class="panel-body"># Nota - ${cliente.nombre} ${cliente.apellidos}</div>

                             <div class="row">
                                <table class="table">
                                  <thead class="thead-light">
                                    <tr>
                                      <th scope="col">Servicio</th>
                                      <th scope="col">Cantidad</th>
                                      <th scope="col">Concepto</th>
                                      <th scope="col">Entrega</th>
                                      <th scope="col">Costo</th>
                                    </tr>
                                  </thead>

                                  <tbody id="tablaResumen">
                                    
                                  </tbody>
                                </table>

                             </div>

                         </div>
                     </div>
                <button onclick="postOrder()" class="btn btn-default">Generar orden</button>
            </div>
                <%--</form>--%>
        </div>
    </div>
    <script>
        <c:import url="/WEB-INF/resources/scripts/recepcion.js"/>
    </script>
</t:wrapper>
